import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

import static java.util.concurrent.TimeUnit.DAYS;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PricerTest {

    private static LocalDateTime tenOfApril() {
        return LocalDate.of(2022, Month.APRIL, 10).atStartOfDay();
    }

    private static LocalDateTime firstOfApril() {
        return LocalDate.of(2022, Month.APRIL, 1).atStartOfDay();
    }

    @Test
    void should_have_an_empty_pricer() {
        Pricer emptyPricer = new EmptyPrice();
        assertEquals(0, emptyPricer.eval(firstOfApril(), tenOfApril()), 0.0001);
    }

    @Test
    void should_have_a_fixed_pricer() {
        Pricer fixedPricer = new FixedPrice(new EmptyPrice(), 12);
        assertEquals(12.0, fixedPricer.eval(firstOfApril(), tenOfApril()), 0.0001);
    }

    @Test
    void should_have_a_pricer_by_time_unit() {
        Pricer pricer = new PriceByTimeUnit(new EmptyPrice(), 2.0, DAYS);
        assertEquals(18.0, pricer.eval(firstOfApril(), tenOfApril()), 0.0001);
    }

    @Test
    void should_stop_a_price_at_end_date() {
        Pricer pricer = new EndAt(new PriceByTimeUnit(new EmptyPrice(), 2.0, DAYS), 3, DAYS);
        assertEquals(6.0, pricer.eval(firstOfApril(), tenOfApril()), 0.0001);
    }

    @Test
    void should_combine_fixed_and_price_by_unit_time() {
        Pricer fixedAndByUnit = new PriceByTimeUnit(new EndAt(new FixedPrice(new EmptyPrice(), 7), 2, DAYS), 3, DAYS);
        assertEquals(7 + 7 * 3, fixedAndByUnit.eval(firstOfApril(), tenOfApril()), 0.0001);

        Pricer byUnitAndFixed = new FixedPrice(new EndAt(new PriceByTimeUnit(new EmptyPrice(), 3, DAYS), 5, DAYS), 20);
        assertEquals(5 * 3 + 20, byUnitAndFixed.eval(firstOfApril(), tenOfApril()), 0.0001);
    }

    @Test
    void fixed_should_absorb_all_duration() {
        Pricer fixedAndByUnit = new PriceByTimeUnit(new FixedPrice(new EmptyPrice(), 7), 3, DAYS);
        assertEquals(7, fixedAndByUnit.eval(firstOfApril(), tenOfApril()), 0.0001);
    }

    @Test
    void price_by_unit_time_should_absorb_all_duration() {
        Pricer byUnitAndFixed = new FixedPrice(new PriceByTimeUnit(new EmptyPrice(), 3, DAYS), 20);
        assertEquals(9 * 3 + 20, byUnitAndFixed.eval(firstOfApril(), tenOfApril()), 0.0001);
    }
}
