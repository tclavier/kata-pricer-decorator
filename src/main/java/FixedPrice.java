import java.time.Duration;
import java.time.LocalDateTime;

public class FixedPrice implements Pricer {
    private Pricer pricer;
    private double value;

    public FixedPrice(Pricer pricer, double value) {
        this.pricer = pricer;
        this.value = value;
    }

    @Override
    public double eval(LocalDateTime start, LocalDateTime end) {
        return value + pricer.eval(start, end);
    }

    @Override
    public Duration consumedDuration(LocalDateTime start, LocalDateTime end) {
        return Duration.between(start, end);
    }
}
