import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

public class PriceByTimeUnit implements Pricer {
    private Pricer pricer;
    private double value;
    private TimeUnit timeUnit;

    public PriceByTimeUnit(Pricer pricer, double value, TimeUnit timeUnit) {
        this.pricer = pricer;
        this.value = value;
        this.timeUnit = timeUnit;
    }

    @Override
    public double eval(LocalDateTime start, LocalDateTime end) {
        double priceBefore = pricer.eval(start, end);
        LocalDateTime newStartDate = start.plus(pricer.consumedDuration(start, end));
        double priceAfter = Duration.between(newStartDate, end).dividedBy(Duration.of(1, timeUnit.toChronoUnit())) * value;
        return priceBefore + priceAfter;
    }

    @Override
    public Duration consumedDuration(LocalDateTime start, LocalDateTime end) {
        return Duration.between(start, end);
    }
}
