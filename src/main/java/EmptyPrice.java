import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class EmptyPrice implements Pricer {
    @Override
    public double eval(LocalDateTime start, LocalDateTime end) {
        return 0;
    }

    @Override
    public Duration consumedDuration(LocalDateTime start, LocalDateTime end) {
        return Duration.of(0, ChronoUnit.DAYS);
    }
}
