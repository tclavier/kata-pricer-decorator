import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

public class EndAt implements Pricer {
    private Pricer pricer;
    private long during;
    private TimeUnit timeUnit;

    public EndAt(Pricer pricer, long during, TimeUnit timeUnit) {
        this.pricer = pricer;
        this.during = during;
        this.timeUnit = timeUnit;
    }

    @Override
    public double eval(LocalDateTime start, LocalDateTime end) {
        if (Duration.between(start, end).dividedBy(Duration.of(1, timeUnit.toChronoUnit())) >= during) {
            LocalDateTime newEndDate = start.plus(consumedDuration(start, end));
            return pricer.eval(start, newEndDate);
        }
        return pricer.eval(start, end);
    }

    @Override
    public Duration consumedDuration(LocalDateTime start, LocalDateTime end) {
        return Duration.of(during, timeUnit.toChronoUnit());
    }
}
