import java.time.Duration;
import java.time.LocalDateTime;

public interface Pricer {
    double eval(LocalDateTime start, LocalDateTime end);

    Duration consumedDuration(LocalDateTime start, LocalDateTime end);
}
